#ifndef _REPOSTAT_H_
#define _REPOSTAT_H_

#include <stdbool.h>
#include <stdatomic.h>
#include "filestat.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct repostat
{
    atomic_int refcnt;

    char *path;
    char *dbpath;

    int monitor_fd;
    filestat_t *files;

    bool is_ready;
    bool is_outdated;

} repostat_t;

repostat_t* repostat_create(const char *path, const char *dbpath);
repostat_t* repostat_newref(repostat_t *self);
void        repostat_release(repostat_t *self);

const char* repostat_get_path(const repostat_t *self);

void repostat_open_file_monitor(repostat_t *self);
void repostat_close_file_monitor(repostat_t *self);

bool repostat_set_status(repostat_t *self, const char *pathname, int status);
bool repostat_get_status(const repostat_t *self, const char *pathname, int *status);

void repostat_set_ready(repostat_t *self);
bool repostat_is_ready(const repostat_t *self);
bool repostat_is_outdated(repostat_t *self);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
