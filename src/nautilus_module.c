#include <gtk/gtk.h>
#include <libnautilus-extension/nautilus-info-provider.h>
#include "config.h"
#include "utils.h"
#include "gitstat.h"
#include "giticon.h"

gitstat_t git;

static GType provider_types[1];

typedef struct async_worker
{
    NautilusInfoProvider *provider;
    NautilusFileInfo *file;
    GClosure *update_complete;
    gchar *filename;

    bool cancelled;

} async_worker_t;

static
async_worker_t* async_worker_create(NautilusInfoProvider *provider,
                                    NautilusFileInfo *file,
                                    GClosure *update_complete,
                                    const char *filename)
{
    async_worker_t *self = g_new(async_worker_t, 1);

    self->provider          = provider;
    self->file              = g_object_ref(file);
    self->update_complete   = g_closure_ref(update_complete);
    self->filename          = g_strdup(filename);

    self->cancelled = false;

    return self;
}

static
void async_worker_release(async_worker_t *self)
{
    g_free(self->filename);
    g_closure_unref(self->update_complete);
    g_object_unref(self->file);
    g_free(self);
}

static
gboolean async_worker_step(async_worker_t *self)
{
    if( self->cancelled )
    {
        async_worker_release(self);
        return G_SOURCE_REMOVE;
    }

    int status = gitstat_query_file_status(&git, self->filename);
    if( status == GITSTAT_CALCULATING )
        return G_SOURCE_CONTINUE;

    const char *icon = giticon_get_statuc_icon(status);
    if( icon )
        nautilus_file_info_add_emblem(self->file, icon);

    nautilus_info_provider_update_complete_invoke(
        self->update_complete,
        self->provider,
        (NautilusOperationHandle*) self,
        NAUTILUS_OPERATION_COMPLETE);

    async_worker_release(self);
    return G_SOURCE_REMOVE;
}

static
async_worker_t* async_worker_create_start(NautilusInfoProvider *provider,
                                          NautilusFileInfo *file,
                                          GClosure *update_complete,
                                          const char *filename)
{
    async_worker_t *worker =
        async_worker_create(provider, file, update_complete, filename);

    if( !g_idle_add((gboolean(*)(gpointer)) async_worker_step, worker) )
    {
        async_worker_release(worker);
        worker = NULL;
    }

    return worker;
}

static
NautilusOperationResult update_file_info(NautilusInfoProvider *provider,
                                         NautilusFileInfo *file,
                                         GClosure *update_complete,
                                         NautilusOperationHandle **handle)
{
    char *fileuri = NULL;

    NautilusOperationResult res = NAUTILUS_OPERATION_COMPLETE;
    do
    {
        fileuri = nautilus_file_info_get_uri(file);
        if( !fileuri ) break;

        char filename[PATH_MAX] = {0};
        if( !utils_name_from_uri(filename, sizeof(filename)-1, fileuri) ) break;

        int status = gitstat_query_file_status(&git, filename);
        if( status == GITSTAT_CALCULATING )
        {
            *handle = (NautilusOperationHandle*)
                async_worker_create_start(provider,
                                          file,
                                          update_complete,
                                          filename);
            res = *handle ?
                NAUTILUS_OPERATION_IN_PROGRESS :
                NAUTILUS_OPERATION_FAILED;
            break;
        }

        const char *icon = giticon_get_statuc_icon(status);
        if( !icon ) break;

        nautilus_file_info_add_emblem(file, icon);

    } while(false);

    if( fileuri )
        g_free(fileuri);

    return res;
}

static
void cancel_update(NautilusInfoProvider *provider,
                   NautilusOperationHandle *handle)
{
    async_worker_t *worker = (async_worker_t*)handle;
    worker->cancelled = true;
}

static
void file_info_provider_iface_init(NautilusInfoProviderIface *iface,
                                   gpointer iface_data)
{
    iface->update_file_info = update_file_info;
	iface->cancel_update    = cancel_update;
}

static
GType module_register_type(GTypeModule *module)
{
    // Register module.

    static const GTypeInfo typeinfo =
    {
        /* interface types, classed types, instantiated types */
        .class_size = sizeof(GObjectClass),

        .base_init = NULL,
        .base_finalize = NULL,

        /* interface types, classed types, instantiated types */
        .class_init = NULL,
        .class_finalize = NULL,
        .class_data = NULL,

        /* instantiated types */
        .instance_size = sizeof(GObject),
        .n_preallocs = 0,
        .instance_init = NULL,

        /* value handling */
        .value_table = NULL,
    };

    GType module_type = g_type_module_register_type(module,
                                                    G_TYPE_OBJECT,
                                                    "NautilusGitIcons",
                                                    &typeinfo,
                                                    0);

    // Add interfaces.

    static const GInterfaceInfo info_provider =
    {
        .interface_init = (void(*)(gpointer,gpointer)) file_info_provider_iface_init,
        .interface_finalize = NULL,
        .interface_data = NULL,
    };

    g_type_module_add_interface(module,
                                module_type,
                                NAUTILUS_TYPE_INFO_PROVIDER,
                                &info_provider);

    return module_type;
}

void nautilus_module_initialize(GTypeModule *module)
{
    GtkIconTheme *theme = gtk_icon_theme_get_default();
    gtk_icon_theme_append_search_path(theme, GITICON_EMBLEMS_PATH);

    gitstat_init(&git);
    provider_types[0] = module_register_type(module);
}

void nautilus_module_shutdown(void)
{
    gitstat_destroy(&git);
}

void nautilus_module_list_types(const GType **types, int *num_types)
{
    *types = provider_types;
    *num_types = G_N_ELEMENTS(provider_types);
}
