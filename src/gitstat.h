#ifndef _GITSTAT_H_
#define _GITSTAT_H_

#include "repolist.h"

#ifdef __cplusplus
extern "C" {
#endif

enum
{
    GITSTAT_CALCULATING = -1,

    GITSTAT_NOT_REPO_FILE = 0,

    GITSTAT_ROOTDIR     = 1 << 0,
    GITSTAT_UNTRACKED   = 1 << 1,
    GITSTAT_CURRENT     = 1 << 2,
    GITSTAT_NEW         = 1 << 3,
    GITSTAT_MODIFIED    = 1 << 4,
    GITSTAT_DELETED     = 1 << 5,
    GITSTAT_RENAMED     = 1 << 6,
    GITSTAT_TYPECHANGE  = 1 << 7,
    GITSTAT_CONFLICTED  = 1 << 8,
    GITSTAT_UNREADABLE  = 1 << 9,
    GITSTAT_IGNORED     = 1 << 10,
};

typedef struct gitstat
{
    repolist_t repolist;
} gitstat_t;

void gitstat_init(gitstat_t *self);
void gitstat_destroy(gitstat_t *self);

int gitstat_query_file_status(gitstat_t *self, const char *filename);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
