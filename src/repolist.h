#ifndef _REPOLIST_H_
#define _REPOLIST_H_

#include <pthread.h>
#include "repostat.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct repolist
{
    void(*repo_builder)(repostat_t *repo);

    struct item     *list;
    pthread_mutex_t list_lock;

} repolist_t;

void repolist_init(repolist_t *self, void(*repo_builder)(repostat_t *repo));
void repolist_destroy(repolist_t *self);

repostat_t* repolist_query_repo(repolist_t *self,
                                const char *root_path,
                                const char *db_path);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
