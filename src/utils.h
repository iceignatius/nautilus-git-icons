#ifndef _UTILS_H_
#define _UTILS_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

char* utils_name_from_uri(char *buf, size_t bufsize, const char *uri);
void utils_name_add_slash_if_dir(char *buf,
                                 size_t bufsize,
                                 const char *fileprefix,
                                 const char *filename);
char* utils_name_remove_leading_chars(char *str, size_t rmcount);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
