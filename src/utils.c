#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include "utils.h"

#define MAX(a,b) ( (a) > (b) ? (a) : (b) )

static
char decode_uri_char(const char **str)
{
    const char *arr = *str;

    unsigned char ch;
    if( arr[0] == '%' && ( arr[1] && isxdigit(arr[1]) ) && ( arr[2] && isxdigit(arr[2]) ) )
    {
        char hexstr[] = { arr[1], arr[2], 0 };
        ch = strtol(hexstr, NULL, 16);
        *str += 3;
    }
    else
    {
        ch = arr[0];
        *str += 1;
    }

    return ch;
}

char* utils_name_from_uri(char *buf, size_t bufsize, const char *uri)
{
    static const char scheme[] = "file://";
    static const size_t scheme_len = sizeof(scheme) - 1;

    if( strncmp(uri, scheme, scheme_len) ) return NULL;

    char *res = buf;

    uri += scheme_len;
    while( bufsize && *uri )
    {
        *buf++ = decode_uri_char(&uri);
        --bufsize;
    }

    if( bufsize ) *buf = 0;

    return res;
}

static
bool is_dir(const char *filename)
{
    struct stat state;
    if( stat(filename, &state) ) return false;

    DIR *fdir = opendir(filename);
    if( fdir ) closedir(fdir);

    return fdir;
}

static
bool is_slash_ended(const char *str)
{
    size_t len = strlen(str);
    return len && str[len-1] == '/';
}

void utils_name_add_slash_if_dir(char *buf,
                                 size_t bufsize,
                                 const char *fileprefix,
                                 const char *filename)
{
    char absfile[PATH_MAX];
    snprintf(absfile,
             sizeof(absfile),
             "%s%s",
             fileprefix ? fileprefix : "",
             filename);

    snprintf(buf,
             bufsize,
             "%s%s",
             filename,
             is_dir(absfile) && !is_slash_ended(absfile) ? "/" : "");
}

char* utils_name_remove_leading_chars(char *str, size_t rmcount)
{
    char *srcpos = str + rmcount;
    char *dstpos = str;

    do
    {
        *dstpos++ = *srcpos++;
    } while( *srcpos );

    *dstpos++ = 0;

    return str;
}
