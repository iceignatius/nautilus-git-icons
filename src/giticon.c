#include "gitstat.h"
#include "giticon.h"

static
const char* get_repo_root_icon(int status)
{
    if( status & (
        GITSTAT_UNTRACKED  |
        GITSTAT_NEW        |
        GITSTAT_MODIFIED   |
        GITSTAT_DELETED    |
        GITSTAT_RENAMED    |
        GITSTAT_TYPECHANGE |
        GITSTAT_CONFLICTED |
        GITSTAT_UNREADABLE ) )
    {
        return "emblem-git-repo-modified";
    }
    else
    {
        return "emblem-git-repo-normal";
    }
}

static
const char* get_repo_file_icon(int status)
{
    if( status & GITSTAT_UNREADABLE )
        return "emblem-git-error";

    if( status & GITSTAT_CONFLICTED )
        return "emblem-git-conflicted";

    if( status & GITSTAT_DELETED )
        return "emblem-git-deleted";

    if( status & ( GITSTAT_MODIFIED | GITSTAT_TYPECHANGE ) )
        return "emblem-git-modified";

    if( status & GITSTAT_RENAMED )
        return "emblem-git-renamed";

    if( status & GITSTAT_NEW )
        return "emblem-git-added";

    if( status & GITSTAT_CURRENT )
        return "emblem-git-normal";

    if( status & GITSTAT_UNTRACKED )
        return "emblem-git-unversioned";

    if( status & GITSTAT_IGNORED )
        return "emblem-git-ignored";

    return NULL;
}

const char* giticon_get_statuc_icon(int status)
{
    if( status == GITSTAT_CALCULATING ) return NULL;
    if( status == GITSTAT_NOT_REPO_FILE ) return NULL;

    return ( status & GITSTAT_ROOTDIR )?
        get_repo_root_icon(status) :
        get_repo_file_icon(status);
}
