#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/inotify.h>
#include "repostat.h"

repostat_t* repostat_create(const char *path, const char *dbpath)
{
    repostat_t *self = NULL;

    bool succ = false;
    do
    {
        if( !( self = malloc(sizeof(*self)) ) )
            break;

        atomic_init(&self->refcnt, 1);
        self->path          = NULL;
        self->dbpath        = NULL;
        self->monitor_fd    = -1;
        self->files         = NULL;

        if( !( self->path = strdup(path) ) )
            break;

        if( !( self->dbpath = strdup(dbpath) ) )
            break;

        if( !( self->files = filestat_create("") ) )
            break;

        self->is_ready      = false;
        self->is_outdated   = false;

        succ = true;
    } while(false);

    if( !succ && self )
    {
        repostat_release(self);
        self = NULL;
    }

    return self;
}

repostat_t* repostat_newref(repostat_t *self)
{
    atomic_fetch_add(&self->refcnt, 1);
    return self;
}

void repostat_release(repostat_t *self)
{
    if( 0 != atomic_fetch_sub(&self->refcnt, 1) - 1 )
        return;

    repostat_close_file_monitor(self);

    if( self->files )
        filestat_release(self->files);

    if( self->dbpath )
        free(self->dbpath);

    if( self->path )
        free(self->path);

    free(self);
}

const char* repostat_get_path(const repostat_t *self)
{
    return self->path;
}

static
void monitor_dirs(int monitor_fd, const char *path)
{
    DIR *dir = opendir(path);
    if( !dir ) return;

    int flags =
        IN_MODIFY |
        IN_ATTRIB |
        IN_CLOSE_WRITE |
        IN_MOVE |
        IN_CREATE |
        IN_DELETE |
        IN_DELETE_SELF |
        IN_MOVE_SELF;
    int wd = inotify_add_watch(monitor_fd, path, flags);
    if( wd < 0 )
        fprintf(stderr,
                "WARNING: Add FS watch failed (errno=%d) on \"%s\"!\n",
                errno,
                path);

    for(struct dirent *entry = readdir(dir);
        entry;
        entry = readdir(dir))
    {
        if( entry->d_type != DT_DIR ) continue;
        if( 0 == strcmp(entry->d_name, ".") ) continue;
        if( 0 == strcmp(entry->d_name, "..") ) continue;

        char subpath[PATH_MAX];
        int len = snprintf(subpath, sizeof(subpath), "%s%s/", path, entry->d_name);
        if( len < 0 || sizeof(subpath) <= len ) continue;

        monitor_dirs(monitor_fd, subpath);
    }

    closedir(dir);
}

void repostat_open_file_monitor(repostat_t *self)
{
    repostat_close_file_monitor(self);

    self->monitor_fd = inotify_init1(IN_NONBLOCK);
    if( self->monitor_fd < 0 )
    {
        fprintf(stderr,
                "WARNING: Open FS watch instance failed! (errno=%d)\n",
                errno);
        return;
    }

    monitor_dirs(self->monitor_fd, self->path);
    if( strncmp(self->dbpath, self->path, strlen(self->path)) )
        monitor_dirs(self->monitor_fd, self->dbpath);
}

void repostat_close_file_monitor(repostat_t *self)
{
    if( self->monitor_fd < 0 ) return;

    close(self->monitor_fd);
    self->monitor_fd = -1;
}

static
const char* extract_path(char *buf, size_t bufsize, const char *str)
{
    if( !(*str) ) return NULL;

    const char *delimiter = strchr(str, '/');
    size_t len = delimiter ? delimiter - str + 1 : strlen(str);
    if( bufsize <= len ) return NULL;

    memcpy(buf, str, len);
    buf[len] = 0;

    return str + len;
}

static
filestat_t* find_or_create_sub_node(filestat_t *node, const char *name)
{
    filestat_t *child = filestat_find_child(node, name);
    if( child ) return child;

    child = filestat_create(name);
    if( !child ) return NULL;

    filestat_add_child(node, child);
    return child;
}

bool repostat_set_status(repostat_t *self, const char *pathname, int status)
{
    filestat_t *node = self->files;
    filestat_set_status(node, filestat_get_status(node) | status);

    const char *pos = pathname;
    char path[strlen(pathname)+1];
    while(( pos = extract_path(path, sizeof(path), pos) ))
    {
        if( !( node = find_or_create_sub_node(node, path) ) )
            return false;

        filestat_set_status(node, filestat_get_status(node) | status);
    }

    return true;
}

bool repostat_get_status(const repostat_t *self, const char *pathname, int *status)
{
    const filestat_t *node = self->files;

    const char *pos = pathname;
    char path[strlen(pathname)+1];
    while(( pos = extract_path(path, sizeof(path), pos) ))
    {
        node = filestat_find_child_c(node, path);
        if( !node ) return false;
    }

    *status = filestat_get_status(node);
    return true;
}

void repostat_set_ready(repostat_t *self)
{
    self->is_ready = true;
}

bool repostat_is_ready(const repostat_t *self)
{
    return self->is_ready;
}

bool repostat_is_outdated(repostat_t *self)
{
    if( self->is_outdated ) return true;
    if( self->monitor_fd < 0 ) return false;

    uint8_t databuf[ sizeof(struct inotify_event) + PATH_MAX ];
    ssize_t datasize = read(self->monitor_fd, databuf, sizeof(databuf));
    if( datasize < 0 )
    {
        if( errno == EAGAIN )
            return false;
        else
            fprintf(stderr, "WARNING: Read FS watch failed! (errno=%d)\n", errno);
    }
    else if( datasize > sizeof(struct inotify_event) )
    {
        self->is_outdated = true;
        repostat_close_file_monitor(self);
    }

    return self->is_outdated;
}
