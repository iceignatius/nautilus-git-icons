#ifndef _GITICON_H_
#define _GITICON_H_

#ifdef __cplusplus
extern "C" {
#endif

const char* giticon_get_statuc_icon(int status);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
