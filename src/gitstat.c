#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <git2.h>
#include "utils.h"
#include "gitstat.h"

static
int translate_git_status(git_status_t status)
{
    int res = 0;

    if( status == GIT_STATUS_CURRENT )          res |= GITSTAT_CURRENT;

    if( status & GIT_STATUS_INDEX_NEW )         res |= GITSTAT_NEW;
    if( status & GIT_STATUS_INDEX_MODIFIED )    res |= GITSTAT_MODIFIED;
    if( status & GIT_STATUS_INDEX_DELETED )     res |= GITSTAT_DELETED;
    if( status & GIT_STATUS_INDEX_RENAMED )     res |= GITSTAT_RENAMED;
    if( status & GIT_STATUS_INDEX_TYPECHANGE )  res |= GITSTAT_TYPECHANGE;

    if( status & GIT_STATUS_WT_NEW )            res |= GITSTAT_UNTRACKED;
    if( status & GIT_STATUS_WT_MODIFIED )       res |= GITSTAT_MODIFIED;
    if( status & GIT_STATUS_WT_DELETED )        res |= GITSTAT_DELETED;
    if( status & GIT_STATUS_WT_RENAMED )        res |= GITSTAT_RENAMED;
    if( status & GIT_STATUS_WT_TYPECHANGE )     res |= GITSTAT_TYPECHANGE;
    if( status & GIT_STATUS_WT_UNREADABLE )     res |= GITSTAT_UNREADABLE;

    if( status & GIT_STATUS_CONFLICTED )        res |= GITSTAT_CONFLICTED;
    if( status & GIT_STATUS_IGNORED )           res |= GITSTAT_IGNORED;

    return res;
}

static
void print_git_error(void)
{
    const git_error *err = git_error_last();
    if( err ) fprintf(stderr, "GIT ERROR (%d): %s\n", err->klass, err->message);
}

static
const char* status_entry_get_currfile(const git_status_entry *entry)
{
    const char *filename = NULL;

    if( !filename && entry->index_to_workdir )
        filename = entry->index_to_workdir->new_file.path;

    if( !filename && entry->head_to_index )
        filename = entry->head_to_index->new_file.path;

    return filename;
}

static
bool parse_status_list(repostat_t *repostat, git_status_list *list)
{
    size_t count = git_status_list_entrycount(list);
    for(size_t i = 0; i < count; ++i)
    {
        const git_status_entry *entry = git_status_byindex(list, i);
        if( !entry ) continue;

        int status = translate_git_status(entry->status);

        const char *root_path   = repostat_get_path(repostat);
        const char *local_file  = status_entry_get_currfile(entry);
        if( !local_file ) continue;

        char filename[PATH_MAX];
        utils_name_add_slash_if_dir(filename,
                                    sizeof(filename),
                                    root_path,
                                    local_file);

        if( !repostat_set_status(repostat, filename, status) ) return false;
    }

    return true;
}

static
void repo_builder(repostat_t *repostat)
{
    static const git_status_options status_opt =
    {
        .version    = GIT_STATUS_OPTIONS_VERSION,
        .show       = GIT_STATUS_SHOW_INDEX_AND_WORKDIR,
        .flags      =
            GIT_STATUS_OPT_INCLUDE_UNTRACKED |
            GIT_STATUS_OPT_INCLUDE_IGNORED |
            GIT_STATUS_OPT_INCLUDE_UNMODIFIED |
            GIT_STATUS_OPT_RENAMES_HEAD_TO_INDEX |
            GIT_STATUS_OPT_RENAMES_INDEX_TO_WORKDIR |
            GIT_STATUS_OPT_RENAMES_FROM_REWRITES |
            GIT_STATUS_OPT_INCLUDE_UNREADABLE
    };

    git_repository *repo = NULL;
    git_status_list *status_list = NULL;

    do
    {
        repostat_open_file_monitor(repostat);
        if( repostat_is_outdated(repostat) )
            break;

        if( git_repository_open(&repo, repostat_get_path(repostat)) )
        {
            print_git_error();
            break;
        }

        if( git_status_list_new(&status_list, repo, &status_opt) )
        {
            print_git_error();
            break;
        }

        if( !parse_status_list(repostat, status_list) )
            break;

    } while(false);

    git_status_list_free(status_list);
    git_repository_free(repo);
}

void gitstat_init(gitstat_t *self)
{
    int res = git_libgit2_init();
    if( res <= 0 ) print_git_error();

    repolist_init(&self->repolist, repo_builder);
}

void gitstat_destroy(gitstat_t *self)
{
    repolist_destroy(&self->repolist);

    int res = git_libgit2_shutdown();
    if( res < 0 ) print_git_error();
}

static
char* adjust_repo_path(const char *filename, size_t *len)
{
    const char pattern1[] = "/.git/modules/";
    char *pos1 = strstr(filename, pattern1);
    if( pos1 )
    {
        *len = sizeof(pattern1) - 1;
        return pos1;
    }

    const char pattern2[] = "/.git/";
    char *pos2 = strstr(filename, pattern2);
    if( pos2 )
    {
        *len = sizeof(pattern2) - 1;
        return pos2;
    }

    return NULL;
}

static
bool get_repo_path(char buf[PATH_MAX], const char *filename)
{
    static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_lock(&mutex);

    git_buf root = {0};

    bool succ = false;
    do
    {
        char filepath[PATH_MAX] = {0};
        strncpy(filepath, filename, sizeof(filepath)-1);
        char *pathend = strrchr(filepath, '/');
        if( pathend ) pathend[1] = 0;

        static char dbpath[PATH_MAX] = {0};
        static char lastpath[PATH_MAX] = {0};

        if( strcmp(filepath, lastpath) )
        {
            if( git_repository_discover(&root, filepath, false, NULL) )
                break;

            strncpy(dbpath, root.ptr, sizeof(dbpath)-1);
            strncpy(lastpath, filepath, sizeof(lastpath)-1);
        }

        memcpy(buf, dbpath, sizeof(dbpath));

        succ = true;
    } while(false);

    git_buf_dispose(&root);

    pthread_mutex_unlock(&mutex);

    return succ;
}

static
bool discover_repo_path(const char *filename,
                        char root_path[PATH_MAX],
                        char db_path[PATH_MAX])
{
    db_path[PATH_MAX-1] = 0;
    if( !get_repo_path(db_path, filename) )
        return false;

    root_path[PATH_MAX-1] = 0;
    strncpy(root_path, db_path, PATH_MAX-1);

    size_t db_len;
    char *db_pos = adjust_repo_path(root_path, &db_len);
    if( db_pos )
        utils_name_remove_leading_chars(db_pos+1, db_len-1);

    return true;
}

static
const char* extract_local_file(const char *root_path, const char *filename)
{
    size_t repo_root_len = strlen(root_path);
    return ( 0 == strncmp(filename, root_path, repo_root_len) )?
        ( filename + repo_root_len ):( NULL );
}

int gitstat_query_file_status(gitstat_t *self, const char *filename)
{
    repostat_t *repo = NULL;

    int status = GITSTAT_NOT_REPO_FILE;
    do
    {
        char normalised_name[PATH_MAX];
        utils_name_add_slash_if_dir(normalised_name,
                                    sizeof(normalised_name),
                                    NULL,
                                    filename);

        char root_path[PATH_MAX], db_path[PATH_MAX];
        if( !discover_repo_path(normalised_name, root_path, db_path) )
            break;

        const char *local_file = extract_local_file(root_path, normalised_name);

        repo = repolist_query_repo(&self->repolist, root_path, db_path);
        if( !repo ) break;

        if( !repostat_is_ready(repo) || repostat_is_outdated(repo) )
        {
            status = GITSTAT_CALCULATING;
            break;
        }

        if( !repostat_get_status(repo, local_file, &status) )
        {
            status = GITSTAT_NOT_REPO_FILE;
            break;
        }

        if( 0 == strlen(local_file) )
            status |= GITSTAT_ROOTDIR;

    } while(false);

    if( repo )
        repostat_release(repo);

    return status;
}
