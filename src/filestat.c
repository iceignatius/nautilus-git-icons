#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "filestat.h"

filestat_t* filestat_create(const char *name)
{
    filestat_t *self = NULL;

    bool succ = false;
    do
    {
        if( !( self = malloc(sizeof(*self)) ) )
            break;

        self->next      = NULL;
        self->children  = NULL;
        self->name      = NULL;
        self->status    = 0;

        if( !( self->name = strdup(name) ) )
            break;

        succ = true;
    } while(false);

    if( !succ && self )
    {
        filestat_release(self);
        self = NULL;
    }

    return self;
}

void filestat_release(filestat_t *self)
{
    filestat_t *child = self->children;
    while( child )
    {
        filestat_t *worker = child;
        child = child->next;

        filestat_release(worker);
    }

    if( self->name )
        free(self->name);

    free(self);
}

void filestat_add_child(filestat_t *self, filestat_t *child)
{
    child->next = self->children;
    self->children = child;
}

filestat_t* filestat_find_child(filestat_t *self, const char *name)
{
    for(filestat_t *child = self->children;
        child;
        child = child->next)
    {
        if( 0 == strcmp(child->name, name) )
            return child;
    }

    return NULL;
}

const filestat_t* filestat_find_child_c(const filestat_t *self, const char *name)
{
    return filestat_find_child((filestat_t*)self, name);
}

const char* filestat_get_name(const filestat_t *self)
{
    return self->name;
}

void filestat_set_status(filestat_t *self, int status)
{
    self->status = status;
}

int filestat_get_status(const filestat_t *self)
{
    return self->status;
}
