#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "repolist.h"

struct item
{
    atomic_int refcnt;

    struct item *prev;
    struct item *next;

    repostat_t *repo;

    void(*builder)(repostat_t*);
};

struct item* item_create(repostat_t *repo, void(*builder)(repostat_t*))
{
    struct item *self = malloc(sizeof(struct item));
    if( !self ) return NULL;

    atomic_init(&self->refcnt, 1);

    self->prev = NULL;
    self->next = NULL;

    self->repo      = repostat_newref(repo);
    self->builder   = builder;

    return self;
}

struct item* item_newref(struct item *self)
{
    atomic_fetch_add(&self->refcnt, 1);
    return self;
}

void item_release(struct item *self)
{
    if( 0 != atomic_fetch_sub(&self->refcnt, 1) - 1 )
        return;

    repostat_release(self->repo);
    free(self);
}

static
void check_pthread_result(int rescode, const char *operation)
{
    if( rescode )
        fprintf(stderr, "PTHREAD ERROR (%d) when doing: %s\n", rescode, operation);
}

static
void repo_builder_default(repostat_t *repo)
{
    // Nothing to do.
}

void repolist_init(repolist_t *self, void(*repo_builder)(repostat_t *repo))
{
    self->repo_builder = repo_builder ? repo_builder : repo_builder_default;
    self->list = NULL;

    check_pthread_result(
        pthread_mutex_init(&self->list_lock, NULL),
        "Initialise list lock.");
}

void repolist_destroy(repolist_t *self)
{
    check_pthread_result(
        pthread_mutex_lock(&self->list_lock),
        "Lock list.");

    struct item *item = self->list;
    while( item )
    {
        struct item *worker = item;
        item = item->next;

        item_release(worker);
    }

    check_pthread_result(
        pthread_mutex_unlock(&self->list_lock),
        "Unlock list.");

    check_pthread_result(
        pthread_mutex_destroy(&self->list_lock),
        "Destroy list lock.");
}

static
void push_item(repolist_t *self, struct item *item)
{
    item_newref(item);

    if( self->list )
        self->list->prev = item;

    item->prev = NULL;
    item->next = self->list;

    self->list = item;
}

static
void erase_item(repolist_t *self, struct item *item)
{
    struct item *prev = item->prev;
    struct item *next = item->next;

    if( prev ) prev->next = next;
    if( next ) next->prev = prev;

    if( !prev ) self->list = next;

    item_release(item);
}

static
void remove_outdated_items(repolist_t *self)
{
    struct item *item = self->list;
    while( item )
    {
        struct item *worker = item;
        item = item->next;

        if( repostat_is_ready(worker->repo) &&
            repostat_is_outdated(worker->repo) )
        {
            erase_item(self, worker);
        }
    }
}

static
struct item* find_item(repolist_t *self, const char *root_path)
{
    for(struct item *item = self->list;
        item;
        item = item->next)
    {
        if( 0 == strcmp(repostat_get_path(item->repo), root_path) )
            return item;
    }

    return NULL;
}

static
void* item_build_thread(struct item *item)
{
    item->builder(item->repo);
    repostat_set_ready(item->repo);

    item_release(item);
    return NULL;
}

static
struct item* add_item_and_build_work(repolist_t *self,
                                     const char *root_path,
                                     const char *db_path)
{
    repostat_t *repo = NULL;
    struct item *item = NULL;

    bool succ = false;
    do
    {
        if( !( repo = repostat_create(root_path, db_path) ) )
            break;

        if( !( item = item_create(repo, self->repo_builder) ) )
            break;

        pthread_t thread;
        if( pthread_create(&thread,
                           NULL,
                           (void*(*)(void*)) item_build_thread,
                           item_newref(item)) )
        {
            item_release(item);
            break;
        }

        pthread_detach(thread);

        push_item(self, item);

        succ = true;
    } while(false);

    if( item )
        item_release(item);

    if( repo )
        repostat_release(repo);

    return succ ? item : NULL;
}

repostat_t* repolist_query_repo(repolist_t *self,
                                const char *root_path,
                                const char *db_path)
{
    check_pthread_result(
        pthread_mutex_lock(&self->list_lock),
        "Lock list.");

    repostat_t *repo = NULL;
    do
    {
        remove_outdated_items(self);

        struct item *item = find_item(self, root_path);
        if( !item ) item = add_item_and_build_work(self, root_path, db_path);
        if( !item ) break;

        repo = repostat_newref(item->repo);

    } while(false);

    check_pthread_result(
        pthread_mutex_unlock(&self->list_lock),
        "Unlock list.");

    return repo;
}
