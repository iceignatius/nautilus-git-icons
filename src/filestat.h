#ifndef _FILESTAT_H_
#define _FILESTAT_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct filestat
{
    struct filestat *next;
    struct filestat *children;

    char    *name;
    int     status;

} filestat_t;

filestat_t* filestat_create(const char *name);
void        filestat_release(filestat_t *self);

void filestat_add_child(filestat_t *self, filestat_t *child);

filestat_t*       filestat_find_child(filestat_t *self, const char *name);
const filestat_t* filestat_find_child_c(const filestat_t *self, const char *name);

const char* filestat_get_name(const filestat_t *self);

void filestat_set_status(filestat_t *self, int status);
int  filestat_get_status(const filestat_t *self);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
