#
# Nautilus extension: Git icons.
#
cmake_minimum_required(VERSION 3.5)
project(nautilus-git-icon)

include(FindPkgConfig)
pkg_check_modules(LIB_NAUTILUS_EXT REQUIRED libnautilus-extension)
pkg_check_modules(LIB_GIT REQUIRED libgit2)

set(GITICON_EMBLEMS_PATH    "share/nautilus-git-icons/emblems")
set(GITICON_EXTEN_PATH      "lib/nautilus/extensions-3.0")

configure_file("${CMAKE_SOURCE_DIR}/config.h.in"
               "${CMAKE_BINARY_DIR}/config.h")

enable_testing()
add_subdirectory(lib)
add_subdirectory(icons)
add_subdirectory(test)
