#
# Test of Git file status module.
#
cmake_minimum_required(VERSION 3.5)
project(gitstat-test)

add_custom_target(sample-repo.tar.xz COMMAND cp "${PROJECT_SOURCE_DIR}/sample-repo.tar.xz" "${PROJECT_BINARY_DIR}")
add_custom_target(sample-repo COMMAND tar -x -f sample-repo.tar.xz DEPENDS sample-repo.tar.xz)

set(srcs ${srcs} ${CMAKE_SOURCE_DIR}/src/utils.c)
set(srcs ${srcs} ${CMAKE_SOURCE_DIR}/src/filestat.c)
set(srcs ${srcs} ${CMAKE_SOURCE_DIR}/src/repostat.c)
set(srcs ${srcs} ${CMAKE_SOURCE_DIR}/src/repolist.c)
set(srcs ${srcs} ${CMAKE_SOURCE_DIR}/src/gitstat.c)
set(srcs ${srcs} ${CMAKE_SOURCE_DIR}/test/gitstat_test.c)

include_directories(${CMAKE_SOURCE_DIR}/src)
include_directories(${CMAKE_BINARY_DIR})
include_directories(${LIB_NAUTILUS_EXT_INCLUDE_DIRS})
include_directories(${LIB_GIT_INCLUDE_DIRS})
link_directories(${LIB_NAUTILUS_EXT_LDFLAGS})
link_directories(${LIB_GIT_LDFLAGS})

set(depend_libs ${depend_libs} ${LIB_NAUTILUS_EXT_LIBRARIES})
set(depend_libs ${depend_libs} ${LIB_GIT_LIBRARIES})

if(CMAKE_COMPILER_IS_GNUCC)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${LIB_NAUTILUS_EXT_CFLAGS_OTHER}")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${LIB_GIT_CFLAGS_OTHER}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${LIB_NAUTILUS_EXT_LDFLAGS_OTHER}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${LIB_GIT_LDFLAGS_OTHER}")
endif()
if(CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${LIB_NAUTILUS_EXT_CFLAGS_OTHER}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${LIB_GIT_CFLAGS_OTHER}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${LIB_NAUTILUS_EXT_LDFLAGS_OTHER}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${LIB_GIT_LDFLAGS_OTHER}")
endif()

add_executable(gitstat-test ${srcs})
target_link_libraries(gitstat-test ${depend_libs})
add_dependencies(gitstat-test sample-repo)
add_test(gitstat-test gitstat-test)
