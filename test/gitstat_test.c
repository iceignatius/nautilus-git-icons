#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include "gitstat.h"

#ifdef NDEBUG
#   undef NDEBUG
#endif

struct targetfile
{
    const char *relfilename;
    int status;
};

static const struct targetfile filelist[] =
{
    { "/etc/fstab",         GITSTAT_NOT_REPO_FILE },
    { "file-not-existed",   GITSTAT_NOT_REPO_FILE },

    { "sample-repo",
        GITSTAT_ROOTDIR |
        GITSTAT_CURRENT |
        GITSTAT_MODIFIED |
        GITSTAT_DELETED |
        GITSTAT_NEW |
        GITSTAT_TYPECHANGE |
        GITSTAT_RENAMED |
        GITSTAT_CONFLICTED |
        GITSTAT_IGNORED |
        GITSTAT_UNTRACKED },
    { "sample-repo/good",           GITSTAT_CURRENT | GITSTAT_IGNORED },
    { "sample-repo/good/current",   GITSTAT_CURRENT },
    { "sample-repo/good/ignored",   GITSTAT_IGNORED },
    { "sample-repo/modified",
        GITSTAT_CURRENT |
        GITSTAT_MODIFIED |
        GITSTAT_DELETED |
        GITSTAT_NEW |
        GITSTAT_TYPECHANGE |
        GITSTAT_RENAMED |
        GITSTAT_CONFLICTED |
        GITSTAT_IGNORED |
        GITSTAT_UNTRACKED },
    { "sample-repo/modified/current",           GITSTAT_CURRENT },
    { "sample-repo/modified/current-in-ignore", GITSTAT_CURRENT },
    { "sample-repo/modified/modified",          GITSTAT_MODIFIED },
    { "sample-repo/modified/deleted",           GITSTAT_DELETED | GITSTAT_UNTRACKED },
    { "sample-repo/modified/newfile",           GITSTAT_NEW },
    { "sample-repo/modified/newfile-in-ignore", GITSTAT_NEW },
    { "sample-repo/modified/type-change",       GITSTAT_TYPECHANGE },
    { "sample-repo/modified/conflicted",        GITSTAT_CONFLICTED },
    { "sample-repo/modified/other",             GITSTAT_UNTRACKED },
    { "sample-repo/modified/rename-new",        GITSTAT_RENAMED },
    { "sample-repo/modified/ignored",           GITSTAT_IGNORED },
    { "sample-repo/empty-dir",                  GITSTAT_IGNORED },
    { "sample-repo/.git",                       GITSTAT_NOT_REPO_FILE },
    { "sample-repo/.gitignore",                 GITSTAT_CURRENT },
    { "sample-repo/.gitmodules",                GITSTAT_CURRENT },

    { "sample-repo/submodule-clean",    GITSTAT_CURRENT },
    { "sample-repo/submodule-clean/repo",
        GITSTAT_ROOTDIR |
        GITSTAT_CURRENT |
        GITSTAT_IGNORED },
    { "sample-repo/submodule-clean/repo/good",
        GITSTAT_CURRENT |
        GITSTAT_IGNORED },
    { "sample-repo/submodule-clean/repo/good/current",  GITSTAT_CURRENT },
    { "sample-repo/submodule-clean/repo/good/ignored",  GITSTAT_IGNORED },
    { "sample-repo/submodule-clean/repo/.git",          GITSTAT_NOT_REPO_FILE },
    { "sample-repo/submodule-clean/repo/.gitignore",    GITSTAT_CURRENT },

    { "sample-repo/submodule-complex",  GITSTAT_MODIFIED },
    { "sample-repo/submodule-complex/repo",
        GITSTAT_ROOTDIR |
        GITSTAT_CURRENT |
        GITSTAT_MODIFIED |
        GITSTAT_DELETED |
        GITSTAT_NEW |
        GITSTAT_TYPECHANGE |
        GITSTAT_RENAMED |
        GITSTAT_IGNORED |
        GITSTAT_UNTRACKED },
    { "sample-repo/submodule-complex/repo/good",
        GITSTAT_CURRENT |
        GITSTAT_IGNORED },
    { "sample-repo/submodule-complex/repo/good/current",    GITSTAT_CURRENT },
    { "sample-repo/submodule-complex/repo/good/ignored",    GITSTAT_IGNORED },
    { "sample-repo/submodule-complex/repo/modified",
        GITSTAT_CURRENT |
        GITSTAT_MODIFIED |
        GITSTAT_DELETED |
        GITSTAT_NEW |
        GITSTAT_TYPECHANGE |
        GITSTAT_RENAMED |
        GITSTAT_IGNORED |
        GITSTAT_UNTRACKED },
    { "sample-repo/submodule-complex/repo/modified/current",            GITSTAT_CURRENT },
    { "sample-repo/submodule-complex/repo/modified/current-in-ignore",  GITSTAT_CURRENT },
    { "sample-repo/submodule-complex/repo/modified/ignored",            GITSTAT_IGNORED },
    { "sample-repo/submodule-complex/repo/modified/modified",           GITSTAT_MODIFIED },
    { "sample-repo/submodule-complex/repo/modified/deleted",            GITSTAT_DELETED | GITSTAT_UNTRACKED },
    { "sample-repo/submodule-complex/repo/modified/newfile",            GITSTAT_NEW },
    { "sample-repo/submodule-complex/repo/modified/newfile-in-ignore",  GITSTAT_NEW },
    { "sample-repo/submodule-complex/repo/modified/type-change",        GITSTAT_TYPECHANGE },
    { "sample-repo/submodule-complex/repo/modified/other",              GITSTAT_UNTRACKED },
    { "sample-repo/submodule-complex/repo/modified/rename-new",         GITSTAT_RENAMED },
    { "sample-repo/submodule-complex/repo/modified/ignored",            GITSTAT_IGNORED },
    { "sample-repo/submodule-complex/repo/.git",                        GITSTAT_NOT_REPO_FILE },
    { "sample-repo/submodule-complex/repo/.gitignore",                  GITSTAT_CURRENT },
};

static const int filecount = sizeof(filelist)/sizeof(filelist[0]);

static
void get_abs_file_name(char *buf, size_t bufsize, const char *cwd, const char *filename)
{
    if( filename[0] == '/' )
        snprintf(buf, bufsize, "%s", filename);
    else
        snprintf(buf, bufsize, "%s/%s", cwd, filename);
}

static
int block_and_get_file_status(gitstat_t *git, const char *filename)
{
    int status;
    do
    {
        status = gitstat_query_file_status(git, filename);
        usleep(1);
    } while( status == GITSTAT_CALCULATING );

    return status;
}

int main(int argc, char *argv[])
{
    char cwd_buf[PATH_MAX] = {0};
    const char *cwd = getcwd(cwd_buf, sizeof(cwd_buf)-1);
    assert( cwd );

    gitstat_t git;
    gitstat_init(&git);

    for(int i=0; i<filecount; ++i)
    {
        char absfilename[4096];
        get_abs_file_name(absfilename, sizeof(absfilename), cwd, filelist[i].relfilename);

        int status = block_and_get_file_status(&git, absfilename);
        printf("%s: %X/%X\n", filelist[i].relfilename, status, filelist[i].status);
        assert( status == filelist[i].status );
    }

    gitstat_destroy(&git);

    return 0;
}
