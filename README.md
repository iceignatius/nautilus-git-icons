# nautilus-gits-icons

Use Git? Use Nautilus?
Why not have your file browser give you info about your repositories?

`nautilus-git-icons` overlays emblems saying whether files are
modified, added, untracked etc.
It marks git repositories as such and displays icons on them showing
whether they have changed files.

## Installation

### Dependence

`nautilus-git-icons` requires `libnautilus-extension` and `libgit2`.
On Ubuntu, these are installable with:

    sudo apt-get install libnautilus-extension-dev libgit2-dev

### Install

    mkdir build && cd build
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr ..
    make
    make test   # Optional.
    make install
    nautilus -q; nautilus & # Restart Nautilus, optional.

### Uninstall

Run the script `sudo ./uninstall.sh`.

## Icon key

Here is what each possible file status looks like,
as well as a few examples of what repositories may look like.
Folders and repositories are marked with the status of their contents,
with the 'worst' status in the index and work tree shown
(not necessarily from the same file).
For files deleted from the work tree,
their status will only be visible via their parent directory,
so this is how they are shown below.

![alt tag](key.png)

## Notes

Icons are updated every time you browse to a directory,
but whilst in a directory,
Nautilus does not ask the extension for new icons
unless it sees a file change on disk.
Tap F5 in Nautilus to force a refresh.
