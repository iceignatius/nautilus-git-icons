#!/bin/sh

remove_icons()
{
	local instdir=/usr/share/nautilus-git-icons

	test -d $instdir && rm -r $instdir
}

remove_module()
{
	local modfile=libnautilus-git-icons.so
	local instdir=/usr/lib/nautilus/extensions-3.0

	test -f $instdir/$modfile && rm $instdir/$modfile
}

remove_module
remove_icons
